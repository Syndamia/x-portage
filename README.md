# garbage

This is my [personal](https://www.funtoo.org/Creating_Your_Own_Overlay) Funtoo overlay. It used to have a crap ton of Gentoo ebuilds, but now I just use the gentoo overlay.

It's not in the [Bitbucket Community Overlays](https://code.funtoo.org/bitbucket/projects/CO) mostly because I don't want people to actually use it, since I don't really know (yet) what I'm doing.

## Install

```bash
sudo eselect repository add garbage git https://gitlab.com/Syndamia/garbage.git
```

Preferred `/etc/portage/repos.conf/eselect-repo.conf` values

```bash
[garbage]
location = /var/db/repos/garbage
sync-type = git
sync-uri = https://gitlab.com/Syndamia/garbage.git
priority = 2
```
