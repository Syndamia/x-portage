# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{7,8,9,10} )

inherit meson python-r1

DESCRIPTION="Nemo python integration"
HOMEPAGE="https://projects.linuxmint.com/cinnamon/ https://github.com/linuxmint/nemo-extensions"
SRC_URI="https://github.com/linuxmint/nemo-extensions/archive/5.2.0.tar.gz -> nemo-extensions-5.2.0.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~riscv ~x86"
IUSE="doc exif +nls selinux tracker xmp"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="
	>=dev-libs/glib-2.14.0
	>=gnome-extra/nemo-2.0.0
"
RDEPEND="
	${DEPEND}
"

S="${WORKDIR}/nemo-extensions-5.2.0/${PN}"

pkg_setup() {
	python_setup
}

src_configure() {
	local emesonargs=(
		$(meson_use exif)
		$(meson_use tracker)
		$(meson_use xmp)
		$(meson_use selinux)
		$(meson_use doc gtk_doc)
	)
	meson_src_configure
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome3_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome3_schemas_update
}
